# Gemello

## Getting started

- Before you start doing anything, youn need to install packages in both **frontend** and **backend** folders.

## Running app

### Development mode

###### Manually

- Go to **/frontend** directory and enter: **npm start**
- Go to **/backend** directory and enter **npm run start:dev**

###### Using docker container

- Open terminal in the root of the application and run these commands:

```
> docker-compose -f docker-compose.yml up --build
```

Next time you run the **docker-compose** you can just type:

```
> docker-compose -f docker-compose.yml up -d
```

Hurray... Your applcation is available on port **4000**. Frontend is available on port **3000** and backend on **8080**.

### Production mode

- Open terminal in the root of the application and run these commands:

```
> docker-compose -f docker-compose.prod.yml up --build
```

Next time you run the **docker-compose** you can just type:

```
> docker-compose -f docker-compose.prod.yml up -d
```

Your applcation is available on port **80**. Backend is available on port **8080**.

import { injectable } from 'inversify'
import dotenv from 'dotenv'

dotenv.config()

interface ApplicationState {
  port?: number;
}

@injectable()
export class Config {
  private app?: ApplicationState

  private getEnvironmentVariables(): ApplicationState {
    return {
      port: Number(process.env.PORT)
    }
  }
  public static init(): Config {
    return new Config()
  }

  public async load () {
    const environmentVariables = this.getEnvironmentVariables()
    this.app = { ...environmentVariables }
  }

  public getApplicationPort(): number {
    return this.app?.port ?? 8080
  }
}

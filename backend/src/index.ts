import 'reflect-metadata'
import { Application } from './Application'

Application
  .init().then((app) => {
    console.log('Application started')
    app.start()
  }, (e) => { console.log('Unexpected error, application terminated', e) })

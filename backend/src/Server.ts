import express, { Application } from 'express'
import { injectable } from 'inversify'
import { Config } from './Config'
import { Routes } from './Routes'

@injectable()
export class Server {
  private app?: Application
  constructor (private readonly config: Config, private readonly routes: Routes) {}

  public run() {
    this.app = express()

    this.routes.registerRoutes(this.app)
    this.app.listen(this.config.getApplicationPort())
  }
}

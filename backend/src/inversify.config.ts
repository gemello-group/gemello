import { Container } from 'inversify'
import { Config } from './Config'

export const container = new Container({ autoBindInjectable: true })

export const bindContainer = () => {
  container.bind<Config>(Config).toSelf().inSingletonScope()
}

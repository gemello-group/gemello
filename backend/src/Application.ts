import { injectable } from 'inversify'
import { Config } from './Config'
import { bindContainer, container } from './inversify.config'
import { Server } from './Server'

@injectable()
export class Application {
  constructor (private readonly server: Server) {}

  public static async init(): Promise<Application> {
    bindContainer()
    const config = container.get(Config)
    await config.load()

    return container.get(Application)
  }

  public start() {
    this.server.run()
  }

  public static stop() {
    console.log('Application stopped')
  }
}

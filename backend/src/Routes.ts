import { Application, Router } from 'express'
import { injectable } from 'inversify'
import path from 'path'
import { router } from './ExpressUtils'

@injectable()
export class Routes {
  public registerRoutes(app: Application) {
    const appRouter = router()
    app.use('/api', this.registerTestRoutes())
  }

  private registerTestRoutes(): Router {
    const testRouter = router()

    testRouter.get('/test', (req, res) => { res.json({ dupa: 'dupa' }) })

    return testRouter
  }
}

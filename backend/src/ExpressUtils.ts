import express, { Router } from 'express'

export const router = (): Router => {
  const expressRouter = express.Router()

  expressRouter.use((req, res, next) => {
    console.log(`Starting request: ${req.baseUrl}`)
    next()
  })

  return expressRouter
}

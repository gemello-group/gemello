# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Crucial External Libraries

* [styled components](https://styled-components.com/) - For apply styles to the components
* [mui](https://mui.com/) - Components library working out of the box
* [react-query](https://react-query.tanstack.com/) - For caching and making api requests